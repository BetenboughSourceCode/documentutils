﻿using Azure.Storage.Files.Shares;
using Azure.Storage.Files.Shares.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BetenboughMerger
{
    public class Program
    {
        private static int ProcessId = Process.GetCurrentProcess().Id;
        public static string SourceFilesDirectory { get; set; }
        private static List<Regex> FileNameRegexes { get; set; }
        private static string DestinationFileName { get; set; }
        private static string LogFilePath { get; set; }

        private static string StorageShareName = ConfigurationManager.AppSettings["StorageShareName"];
        private static string StorageAccountConnectionString = ConfigurationManager.AppSettings["StorageAccountConnectionString"];

        private static ShareClient _ShareClient;

        /// <summary>
        /// Arguments:
        /// 1. List of file names to be merged
        /// 2. Output file name
        /// 3. [Optional] Log file name
        /// </summary>
        public static void Main(string[] args)
        {
            if (HelpRequired(args))
            {
                DisplayHelp();
            }
            else
            {
                // Validation
                if (args.Length < 3)
                {
                    WriteLog("Insufficient number of paramaters. Type -h for help.", true);
                    return;
                }
                else
                {
                    var arguments = string.Join(Environment.NewLine, args).Trim();
                    WriteLog($"Starting with arguments: {arguments}");
                }

                // Do the work
                SourceFilesDirectory = args[0];
                FileNameRegexes = args[1].Split(',').Select(name => new Regex(name.Trim())).ToList();
                DestinationFileName = args[2];
                LogFilePath = args.Length > 3 ? args[3].Trim() : string.Empty;

                WriteLog("BetenboughMerger program started.");
                WriteLog($"Source directory is: {args[0]}");
                WriteLog($"File name regexes are: {args[1]}");
                WriteLog($"Destination file name is: {args[2]}");
                WriteLog(LogFilePath == string.Empty ? "Log path not specified; logs will not be written." : $"Log file is: {LogFilePath}");

                Merge();
            }
        }

        private static bool HelpRequired(string[] args)
        {
            return args.Contains("-h") || args.Contains("--help") || args.Contains("/?");
        }

        private static void DisplayHelp()
        {
            WriteLog("This program accepts 4 arguments in this respective order:");
            WriteLog("0. Source directory containing files to be merged");
            WriteLog("1. List of file name regexes matching files to be merged");
            WriteLog("2. Merge destination file name");
            WriteLog("3. [Optional] Log file name");
            WriteLog("");
        }

        private static void WriteLog(string logMessage, bool writeToFile = false)
        {
            Console.WriteLine(logMessage);

            if (writeToFile)
            {
                if (string.IsNullOrEmpty(LogFilePath))
                    return;

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append($"{DateTime.Now.ToString("yyyyMMddHHmmss")} {ProcessId} > ");
                stringBuilder.AppendLine(logMessage);

                CreateDirectory(Path.GetDirectoryName(LogFilePath));
                File.AppendAllText(LogFilePath, stringBuilder.ToString());
            }
        }

        private static void CreateDirectory(string path)
        {
            if (!string.IsNullOrEmpty(path) && !Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Merge multiple PDF files into one single file.
        /// Provide a log file name if you want to log the merging, leave it blank if you don't want to write log.
        /// </summary>
        private static void Merge()
        {
            WriteLog($"Beginning PDF merge...");

            try
            {
                // Open a connection to the Azure storage account
                _ShareClient = new ShareClient(StorageAccountConnectionString, StorageShareName);
                var directory = _ShareClient.GetDirectoryClient(SourceFilesDirectory);

                // Get the output directory ready.
                PrepareOutputDirectory(directory);

                // Merge the source PDF files into the destination PDF.                
                MergeFiles(directory);
            }
            catch (Exception ex)
            {
                WriteLog($"Error occurred: {ex.Message}\nStack Trace: {ex.StackTrace}", true);
            }
        }

        /// <summary>
        /// Prepares the output directory by creating it if it doesn't exist or deleting an existing output file if it already exists in the target output directory.
        /// </summary>
        private static void PrepareOutputDirectory(ShareDirectoryClient directory)
        {
            WriteLog($"Preparing output directory: {DestinationFileName}");

            if (!directory.Exists())
            {
                WriteLog($"Creating a directory for the output file at: {SourceFilesDirectory}");
                directory.Create();
            }

            var fileClient = directory.GetFileClient(DestinationFileName);

            // If the file does not exist, make sure the directory for it be created.
            if (fileClient.Exists())
            {
                WriteLog($"Deleting existing output file at: {DestinationFileName}");
                fileClient.Delete();
            }
        }

        /// <summary>
        /// Attempts to merge the source files into the destination pdf.
        /// </summary>
        private static void MergeFiles(ShareDirectoryClient directory)
        {
            // Get list of files in directory 
            var fileNames = directory.GetFilesAndDirectories().Where(f => !f.IsDirectory).Select(f => f.Name).ToList();

            /// If there are no files to merge, we have to exit the method here, otherwise an exception will be thrown
            /// when we close the destination file (destPdf), and a broken PDF will be created, which we don't want
            /// that to happen.
            
            if (!AnyFilesToMerge(fileNames))
                return;

            WriteLog("Merging files...");
            using (var destinationStream = new MemoryStream())
            {
                using (var document = new Document())
                {
                    using (var copy = new PdfCopy(document, destinationStream))
                    {
                        document.Open();
                        copy.Open();

                        foreach (var fileNameRegex in FileNameRegexes)
                        {
                            var targetFiles = GetTargetSourceFiles(fileNameRegex, fileNames);

                            if (targetFiles == null)
                                continue;
                            foreach (var targetFile in targetFiles)
                            {
                                try
                                {
                                    // Download the file from Azure storage
                                    var fileClient = directory.GetFileClient(targetFile);
                                    ShareFileDownloadInfo download = fileClient.Download();

                                    using (var memoryStream = new MemoryStream())
                                    {
                                        download.Content.CopyTo(memoryStream);
                                        memoryStream.Position = 0;

                                        // Merge the file into the destination file
                                        var reader = new PdfReader(memoryStream);
                                        copy.AddDocument(reader);
                                        reader.Close();
                                    }
                                    WriteLog($"Merged {targetFile}!");

                                    // remove the merged file from list of files to ensure no duplicates 
                                    fileNames.Remove(targetFile);
                                }
                                catch (PdfException pdfException)
                                {
                                    throw new PdfException($"Something went wrong when reading the file '{targetFile}'. {pdfException}");
                                }
                            }
                        }
                        copy.CloseStream = false;

                    }

                    // Now upload the combined file to Azure storage
                    destinationStream.Position = 0;
                    var destinationFileClient = directory.GetFileClient(DestinationFileName);
                    destinationFileClient.Create(destinationStream.Length);

                    // Break it into chunks since the Upload allows a maximum of 4 MiB
                    var maxBlockSize = 4194304;
                    long offset = 0;
                    var binaryReader = new BinaryReader(destinationStream);
                    while (offset < destinationStream.Length)
                    {
                        var buff = binaryReader.ReadBytes(maxBlockSize);

                        var uploadChunk = new MemoryStream();
                        uploadChunk.Write(buff, 0, buff.Length);
                        uploadChunk.Position = 0;
                        destinationFileClient.UploadRange(new Azure.HttpRange(offset, buff.Length), uploadChunk);
                        offset += buff.Length;
                    }
                    binaryReader.Close();
                }
            }

            WriteLog($"Finished merging source files into {Path.GetFileName(DestinationFileName)}!");
        }

        /// <summary>
        /// Check the input folder to see if there are any files to merge.
        /// </summary>
        private static bool AnyFilesToMerge(List <string> fileNames)
        {
            foreach (var fileNameRegex in FileNameRegexes)
            {
                if (GetTargetSourceFiles(fileNameRegex, fileNames) != null)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the file that should be merged by matching its path with the input parameter.
        /// </summary>        
        private static List<string> GetTargetSourceFiles(Regex fileNameRegex, List<string> sourceFileNames)
        {
            WriteLog($"Locating files with name matching the regular expression: {fileNameRegex.ToString()}");

            // Gets list of files where the regex matches 
            List<string> matchingFiles = sourceFileNames.Where(fileName => fileNameRegex.IsMatch(Path.GetFileName(fileName))).ToList();
            var matchingFileCount = matchingFiles.Count();
            if (matchingFileCount == 1)
            {
                WriteLog($"Found '{Path.GetFileName(matchingFiles.Single())}', attempting to merge... ");
            }
            else if (matchingFileCount == 0)
            {
                WriteLog("None found, skipping...");
                return null;
            }
            else if (matchingFileCount > 1)
            {
                WriteLog($"Found {matchingFileCount} files: {string.Join(",", matchingFiles.Select(f => $"'{Path.GetFileName(f)}'"))}, attempting to merge... ");
               
            }
            return matchingFiles;
        }
    }
}
