﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Xunit.Sdk;

namespace BetenboughMerger.Tests
{
    public class TestDirectoryAttribute : BeforeAfterTestAttribute
    {
        public string WorkingDirectory { get; set; }
        public List<string> OriginalFiles { get; set; }

        public TestDirectoryAttribute(string workingDirectory)
        {
            WorkingDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,workingDirectory);
        }

        public override void Before(MethodInfo methodUnderTest)
        {
            Directory.SetCurrentDirectory(WorkingDirectory);            
            OriginalFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories).ToList();
            base.Before(methodUnderTest);
        }

        public override void After(MethodInfo methodUnderTest)
        {
            Directory.SetCurrentDirectory(WorkingDirectory);
            var newFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories).Except(OriginalFiles);
            foreach (var file in newFiles)
            {
                File.Delete(file);
            }
            base.After(methodUnderTest);
        }        
    }
}