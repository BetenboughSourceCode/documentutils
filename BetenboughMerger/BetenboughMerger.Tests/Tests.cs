﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Xunit;

namespace BetenboughMerger.Tests
{
    public class Tests
    {
        private const string TestDirectoryName = "TestDirectory";
        private static string TestDirectoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TestDirectoryName);
        private static string OutputFileName = "test-merge.pdf";

        [Fact]
        [TestDirectory(TestDirectoryName)]
        public void Main_SourceFileRegexesMatchFileNamesExactly_MergesFiles()
        {
            /// Arrange
            // Set the input source file names
            var sourceFileRegexes = new List<string>
            {
                "^A-1\\.1-w\\.pdf",
                "^A-1\\.10 x\\.pdf",
                "^AB-2\\.1 - y\\.pdf",
                "^B-2\\.1 - y\\.pdf"
            };

            /// Act
            // Run the merge program.
            Program.Main(new string[] { TestDirectoryPath, string.Join(",", sourceFileRegexes), OutputFileName, "Log.txt" });

            // Get a count of the pages.
            var mergedPagesCount = CountPDFPages(OutputFileName);

            /// Assert
            // Verify that the number of pages in the merged pdf match the number of source pages.
            Assert.Equal(sourceFileRegexes.Count, mergedPagesCount);
        }

        [Fact]
        [TestDirectory(TestDirectoryName)]
        public void Main_MultipleRegexesMatchSingleFile_MergesFileOnlyOnce()
        {
            /// Arrange 
            // Give two identical regex values that match the same file 
            var sourceFileRegexes = new List<string>
            {
                "^B-2\\.1[^\\d].*",
                "^B-2\\.1[^\\d].*"
            };
            /// Act
            // Run merge 
            Program.Main(new string[] { TestDirectoryPath, string.Join(",", sourceFileRegexes), OutputFileName, "Log.txt" });

            // Get count of pages
            var mergedPagesCount = CountPDFPages(OutputFileName); 
            
            /// Assert 
            // Ensure that file is merged only once 
            Assert.Equal(1,mergedPagesCount);
        }
        
        [Fact]
        [TestDirectory(TestDirectoryName)]
        public void Main_SourceFileRegexesDoNotMatchAnyFiles_NoFilesMerged()
        {
            /// Arrange
            // Set the input source file names
            var sourceFileRegexes = new List<string>
            {
                "abc",
                "def",
                "ghi"
            };

            /// Act
            // Run the merge program.
            Program.Main(new string[] { TestDirectoryPath, string.Join(",", sourceFileRegexes), OutputFileName, "Log.txt" });

            // Get a count of the pages.
            var mergedPagesCount = CountPDFPages(OutputFileName);

            /// Assert
            // Verify that no pages were merged.
            Assert.Equal(0, mergedPagesCount);
        } 

        [Fact]
        [TestDirectory(TestDirectoryName)]
        public void Main_SourceFileRegexMatchesTwoFiles_BothFilesMerged()
        {
            /// Arrange
            // Give Regex that matches two files in the test directory 
            var sourceFileRegexes = new List<string>
            {
                "^A-1\\.10.*",
            };

            /// Act
            // Run the merge program.
            Program.Main(new string[] { TestDirectoryPath, string.Join(",", sourceFileRegexes), OutputFileName, "Log.txt" });

            // Get a count of the pages.
            var mergedPagesCount = CountPDFPages(OutputFileName);

            /// Assert
            // Verify that the number of pages in the merged pdf match the number of known matches.
            Assert.Equal(sourceFileRegexes.Count*2, mergedPagesCount);
        }

        [Fact]
        [TestDirectory(TestDirectoryName)]
        public void Main_NotAllSourceFileRegexHaveMatches()
        {
            /// Arrange 
            // Set regex where one has no matches and one has a single match 
            var sourceFileRegexes = new List<string>
            {
                "notAMatch",
                "^A-1\\.1-w\\.pdf"

            };

            /// Act
            // Run Merge 
            Program.Main(new string[] { TestDirectoryPath, string.Join(",", sourceFileRegexes), OutputFileName, "Log.txt" });
            
            // Count the pages 
            var mergedPagesCount = CountPDFPages(OutputFileName);

            /// Assert 
            // Verify there is only one page in the pdf
            Assert.Equal(1, mergedPagesCount);

        }

        private static int CountPDFPages(string filepath)
        {
            if (File.Exists(filepath))
            {
                var file = File.ReadAllText(filepath);
                var rgx = new Regex(@"<<.*Count (\d+).*/Type/Pages>>");
                var match = rgx.Match(file);
                if (!match.Success)
                    return 0;
                var count = int.Parse(match.Groups[1].Value);
                return count;
            }
            else
            {
                return 0;
            }
        }
    }
}
